# 升鲜宝   生鲜配送系统企业应用版功能简要

#### 介绍
升鲜宝供应链管理系统功能简介，商业化产品的使用场景及功能简介。

#### 软件架构
软件采用C/A/S （Server数据库+应用程序服务器(WCF)+Client客户端形式）


#### 安装教程

1.  .net4.0 以上的版本
2.  锐狼报表


#### 使用说明

1.  数据库部署
2.  服务端部署
3.  客户端部署

#### 参与贡献

*.  本产品由升鲜宝供应链系统团队开发，有偿商业化使用。


界面展示：

   1.登录
    ![登录](https://images.gitee.com/uploads/images/2021/1108/123225_8d2800fd_9013.png "屏幕截图.png")

   2.主界面
    ![主界面](https://images.gitee.com/uploads/images/2021/1108/123321_ecadae8a_9013.png "屏幕截图.png")

   3.单位管理
    ![单位管理](https://images.gitee.com/uploads/images/2021/1108/123542_dc9acf1d_9013.png "屏幕截图.png")
   
   4.新增商品单位
    ![新增商品单位](https://images.gitee.com/uploads/images/2021/1108/123607_8bb2d4ec_9013.png "屏幕截图.png")

   5.修改商品单位
    ![修改商品单位](https://images.gitee.com/uploads/images/2021/1108/123738_bcb4f3de_9013.png "屏幕截图.png")

   6.商品品牌管理
    ![商品品牌](https://images.gitee.com/uploads/images/2021/1108/124250_b806fc18_9013.png "屏幕截图.png")

   7.商品品牌修改
    ![商品品牌修改](https://images.gitee.com/uploads/images/2021/1108/124329_09aabfa3_9013.png "屏幕截图.png")

   8.商品加工方式
    ![商品加工方式](https://images.gitee.com/uploads/images/2021/1108/124641_355602c7_9013.png "屏幕截图.png")

   9.商品类别
    ![商品类别](https://images.gitee.com/uploads/images/2021/1108/124901_3d5dd8fd_9013.png "屏幕截图.png")
  
   10.新增商品类别
    ![新增商品类别](https://images.gitee.com/uploads/images/2021/1108/124933_805836f7_9013.png "屏幕截图.png")

   
   11.商品资料管理
    ![商品资料管理](https://images.gitee.com/uploads/images/2021/1108/125135_fe00e930_9013.png "屏幕截图.png")
   
   12.商品资料新增
    ![商品资料新增](https://images.gitee.com/uploads/images/2021/1108/125334_9873016b_9013.png "屏幕截图.png")
   13.商品资料修改
    ![商品资料修改](https://images.gitee.com/uploads/images/2021/1108/125634_a32d6225_9013.png "屏幕截图.png")
   14.商品定义销售价
    ![商品定义销售价](https://images.gitee.com/uploads/images/2021/1108/125729_ec714430_9013.png "屏幕截图.png")
  
   15.批量更新采购价
   ![批量更新采购价](https://images.gitee.com/uploads/images/2021/1108/130217_4612e963_9013.png "屏幕截图.png")
   
   16.今日待检测商品列表
   ![今日待检测商品列表](https://images.gitee.com/uploads/images/2021/1108/130331_4976b849_9013.png "屏幕截图.png")

   17.检测信息列表
   ![检测信息录入](https://images.gitee.com/uploads/images/2021/1108/130431_551ed5e9_9013.png "屏幕截图.png")
   18.商品检测信息录入
   ![商品检测信息录入](https://images.gitee.com/uploads/images/2021/1108/130515_5838dea0_9013.png "屏幕截图.png")
   
   19.价格组管理
   ![价格组管理](https://images.gitee.com/uploads/images/2021/1108/130620_d2c6b62b_9013.png "屏幕截图.png")

   20.价格组修改
   ![价格组修改](https://images.gitee.com/uploads/images/2021/1108/151645_eaaf7777_9013.png "屏幕截图.png")


   21.价格组复制
  ![价格组复制](https://images.gitee.com/uploads/images/2021/1108/151723_707fe891_9013.png "屏幕截图.png")
   
   22.价格组邀请码
   ![价格组邀请码](https://images.gitee.com/uploads/images/2021/1108/151806_736a9186_9013.png "屏幕截图.png")

   23.价格组毛利规则与设置
   ![价格组毛利规则与设置](https://images.gitee.com/uploads/images/2021/1108/151846_5c56a213_9013.png "屏幕截图.png")

   24.新增价格组毛利规则
   ![新增价格组毛利规则](https://images.gitee.com/uploads/images/2021/1108/151914_9ace7a0e_9013.png "屏幕截图.png")


   25.客户管理
   ![客户管理](https://images.gitee.com/uploads/images/2021/1108/152112_c24ec8e5_9013.png "屏幕截图.png") 
   26.新增客户
   ![新增客户](https://images.gitee.com/uploads/images/2021/1108/152142_78f429f0_9013.png "屏幕截图.png")
   27.修改客户
   ![修改客户信息](https://images.gitee.com/uploads/images/2021/1108/152237_ae4cc4f0_9013.png "屏幕截图.png")
   28.客户的特殊价格
   ![客户的特殊价格](https://images.gitee.com/uploads/images/2021/1108/152313_db3ffd72_9013.png "屏幕截图.png")

   29.价格组商品价格
   ![价格组商品价格](https://images.gitee.com/uploads/images/2021/1108/152416_b96312e1_9013.png "屏幕截图.png")
   30.价格组商品价格修改
   ![价格组商品价格修改](https://images.gitee.com/uploads/images/2021/1108/152501_e45bed30_9013.png "屏幕截图.png")
   31.价格组商品价格列表查询
   ![价格组商品价格列表查询](https://images.gitee.com/uploads/images/2021/1108/152618_92a6dec1_9013.png "屏幕截图.png")
   32.批量更新价格组销售价
   ![批量更新价格组销售价](https://images.gitee.com/uploads/images/2021/1108/152715_8cbc5dc9_9013.png "屏幕截图.png")
   33.新增客户订单（客服后台代客户下单）
   ![新增客户订单（客服后台代客户下单）](https://images.gitee.com/uploads/images/2021/1108/152817_4d2df987_9013.png "屏幕截图.png")
   34.销售订单新增，选择客户，输入商品
   ![销售订单新增，选择客户，输入商品](https://images.gitee.com/uploads/images/2021/1108/153001_0a4e76f8_9013.png "屏幕截图.png")
   35.销售订单新增，选择客户，输入商品
   ![销售订单新增，选择客户，输入商品](https://images.gitee.com/uploads/images/2021/1108/153059_8c5d08c3_9013.png "屏幕截图.png")

   36.销售订单新增，保存的时候，商品亏本，系统会提示
   ![销售订单新增，保存的时候，商品亏本，系统会提示](https://images.gitee.com/uploads/images/2021/1108/153338_0572c9b7_9013.png "屏幕截图.png")
   
   37.销售订单审核
   ![销售订单审核](https://images.gitee.com/uploads/images/2021/1108/153738_1c992d56_9013.png "屏幕截图.png")

   38.销售订单汇总
   ![销售订单汇总](https://images.gitee.com/uploads/images/2021/1108/154027_998de7fb_9013.png "屏幕截图.png")
   
   39.销售订单汇总导出格式01
   ![销售订单汇总导出格式01](https://images.gitee.com/uploads/images/2021/1108/154104_d54ec205_9013.png "屏幕截图.png")
   
   40.销售订单汇总导出格式02
   ![销售订单汇总导出格式02](https://images.gitee.com/uploads/images/2021/1108/154220_20a1f091_9013.png "屏幕截图.png")

   41.销售订单发货01
   ![销售订单发货](https://images.gitee.com/uploads/images/2021/1108/154433_fc99765a_9013.png "屏幕截图.png")

   42.销售订单发货02，对货单
   ![销售订单发货02，对货单](https://images.gitee.com/uploads/images/2021/1108/154536_f94c3c24_9013.png "屏幕截图.png")
  
   43.销售订单实发操作03
   ![销售订单实发操作03](https://images.gitee.com/uploads/images/2021/1108/154618_82e13fe0_9013.png "屏幕截图.png")

   44.销售订单实发操作04，发货的时候，客户临时加单
   ![销售订单实发操作04，发货的时候，客户临时加单01](https://images.gitee.com/uploads/images/2021/1108/154737_8029edb4_9013.png "屏幕截图.png")
   ![销售订单实发操作04，发货的时候，客户临时加单02](https://images.gitee.com/uploads/images/2021/1108/154835_6e9a0ea7_9013.png "屏幕截图.png")
   ![销售订单实发操作05](https://images.gitee.com/uploads/images/2021/1108/154939_9c88d1cc_9013.png "屏幕截图.png")

   45.销售订单打配送单操作
   ![销售订单打配送单操作](https://images.gitee.com/uploads/images/2021/1108/155349_f177a7a3_9013.png "屏幕截图.png")
   ![打印模板的选择](https://images.gitee.com/uploads/images/2021/1108/155439_af58f297_9013.png "屏幕截图.png")
   ![打印模板设计](https://images.gitee.com/uploads/images/2021/1108/155555_85a5ebed_9013.png "屏幕截图.png")
    

  

   

   
  
   
 
